<?php


class User
{

    //Creación de la variable $db
    private $db;

    //Constructor
    public function __construct()
    {

        //Instancia de la base de datos
        $this->db = new Database();
    }

    /*----------------------------------
            -  OBTENER USUARIO MEDIANTE EMAIL  -
            -----------------------------------*/
    public function findUserByEmail($email)
    {


        //Query para encontrar al usuario por email
        $this->db->query('SELECT email FROM users WHERE email=:email');


        $this->db->bind(':email', $email);

        //Devuelta de los datos del usuario
        $count = $this->db->single("User");

        //Checkeo de que se ha hecho la búsqueda
        if ($count) {

            return true;
        } else {

            return false;
        }
    }

    /*---------------------------
            -     REGISTRO USUARIO      -
            ----------------------------*/

    public function register($data)
    {

        //Query para insertar los datos del usuario en la tabla users
        $this->db->query("INSERT INTO users(name,email,password,phone) VALUES(:name,:email,:password,:phone);");

        //Bindeo de las variables a introducir
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        $this->db->bind(':phone', $data['phone']);

        //Ejecución de la consulta
        $result = $this->db->execute();

        //Checkeo de que se ha hecho la inserción
        if ($result) {

            return true;
        } else {

            return false;
        }
    }

    /*---------------------------
            -     LOGIN USUARIO     -
            ----------------------------*/
    public function login($user_email, $user_password)
    {

        $this->db->query("SELECT * FROM users WHERE email=:email;");
        $this->db->bind(':email', $user_email);
        $user = $this->db->single("User");
        if ($user->password == false) {
        }


        if (password_verify($user_password, $user->password)) {

            return $user;
        }
    }

    public function edit($data)
    {

        $this->db->query('UPDATE users SET name=:name,email=:email,phone=:phone,password=:password WHERE id=:id');
        $this->db->bind(':name', $data['name_new']);
        $this->db->bind(':password', $data['password_new']);
        $this->db->bind(':email', $data['email_new']);
        $this->db->bind(':phone', $data['phone_new']);
        $this->db->bind(':id',$data['id']);

        $updated=$this->db->execute();

        if($updated){

            return true;
        }else{

            return false;
        }
    }

    /*---------------------------
            -  OBTENER USUARIO POR ID   -
            ----------------------------*/
    public function getUserById($id)
    {

        //Query para obtener todos los datos del usuario mediante id
        $this->db->query("SELECT * from users where id=:id");
        $this->db->bind(':id', $id);
        //Consulta para devolver el usuario
        $user = $this->db->single('User');

        //Devuelta de los datos
        return $user;
    }

    public function checkUserRout($id)
    {
        $this->db->query("SELECT * from routdetails where rout_users_id=:id");
        $this->db->bind(':id', $id);
        $checked = $this->db->single('User');

        return $checked;
    }

    public function deleteUser($id)
    {

        $this->db->query("DELETE from users where id=:id");
        $this->db->bind(':id', $id);
        $this->db->execute();
    }
}
