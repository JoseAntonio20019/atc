<?php

class Rout
{

    private $db;

    public function __construct()
    {

        $this->db = new Database();
    }

    /*---------------------------
    -     OBTENER RUTAS      -
    ----------------------------*/
    
    public function getRoutes()
    {

        //Query para obtener todas las rutas
        $this->db->query('SELECT *
        FROM rout r 
        INNER JOIN users u
        ON r.users_id = u.id');
        $results = $this->db->resultSet('Rout');
        //Valor a devolver
        return $results;
    }

    

    /*---------------------------
    -       AÑADIR RUTAS        -
    ----------------------------*/

    public function addRoutes($data)
    {

        //Query para añadir en cada espacio a la tabla rout
        $this->db->query("INSERT INTO rout(inicio,destino,plazas,gastos,foto,users_id) values(:inicio,:destino,:plazas,:gastos,:image,:users_id)");

        //Bindeos de cada uno de los valores a introducir
        $this->db->bind(':inicio', $data['inicio']);
        $this->db->bind(':destino', $data['destino']);
        $this->db->bind(':plazas', $data['plazas']);
        $this->db->bind(':gastos', $data['gastos']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':users_id', $data['user_id']);
        //Ejecución de la consulta
        $result = $this->db->execute();

        //Comprobación de que se ha hecho la consulta
        if ($result) {

            return true;
        } else {

            return false;
        }
    }

    /*---------------------------
     -  AÑADIR USUARIO A RUTA    -
    ----------------------------*/

    public function addUserRout($data)
    {
        //Query para la inserción de un usuario y su ruta asociada a la tabla routdetails
        $this->db->query('INSERT INTO routdetails(rout_id,rout_users_id) values (:routid,:userid)');

        //Bindeo de las variables a introducir
        $this->db->bind(':routid', $data['rout']->id_rout);
        $this->db->bind(':userid', $data['user']->id);
        //Ejecución de la consulta
        $result = $this->db->execute();

        //Comprobación de que se ha hecho la consulta
        if ($result) {

            return true;
        } else {

            return false;
        }
    }

    /*---------------------------
    -       DETALLES DE LAS RUTAS        -
    ----------------------------*/
    public function getRoutDetails($id)
    {

        //Query para la obtención de los usuarios de la ruta seleccionada por id
        $this->db->query('SELECT * FROM routdetails INNER JOIN users ON routdetails.rout_users_id=users.id WHERE routdetails.rout_id=:id');
        $this->db->bind(':id', $id);
        //Devuelta de toos los valores posibles
        $result = $this->db->resultSet('Rout');

        //Devuelta de los resultados de la segunda consulta.
        return $result;
    }


    /*---------------------------------
    - OBTENER RUTAS POR ID DE USUARIO -
    ---------------------------------*/
    public function getRoutesByUserId($id)
    {
        //Query para obtener las rutas mediante el id de usuario
        $this->db->query('SELECT * FROM rout INNER JOIN users ON users.id = rout.users_id WHERE users.id = :id');
        $this->db->bind(':id', $id);
        //Devuelta de toos los valores posibles
        $results = $this->db->resultSet('Rout');

        //Devuelta de los resultados de la segunda consulta.
        return $results;
    }

    /*----------------------------------
    - OBTENER INFORMACIÓN DE LAS RUTAS -
    ----------------------------------*/

    public function getRoutesInfoById($id){

         //Query para obtener todos los detalles de las rutas mediante el id de usuario
         $this->db->query('SELECT * FROM rout INNER JOIN routdetails ON routdetails.rout_id = rout.id_rout INNER JOIN users on rout.users_id=users.id WHERE routdetails.rout_users_id = :id');
         $this->db->bind(':id', $id);
         //Devuelta de todos los valores posibles
         $results = $this->db->resultSet('Rout');
 
         //Devuelta de los resultados de la segunda consulta.
         return $results;
    }

     /*---------------------------
    -    OBTENER RUTA POR ID DE USUARIO        -
    ----------------------------*/

    public function getRoutByUserId($id)
    {

        //Query para obtener la ruta mediante el id de usuario
        $this->db->query('SELECT * FROM rout INNER JOIN users ON users.id = rout.users_id WHERE users.id = :id');
        $this->db->bind(':id', $id);
        //Devuelta de toos los valores posibles
        $results = $this->db->single('Rout');

        //Devuelta de los resultados de la segunda consulta.
        return $results;
    }

    public function getRoutDetailsByUserId($id)
    {

        //Query para obtener la ruta mediante el id de usuario
        $this->db->query('SELECT * FROM routdetails INNER JOIN users ON users.id = routdetails.rout_users_id WHERE users.id = :id');
        $this->db->bind(':id', $id);
        //Devuelta de toos los valores posibles
        $results = $this->db->single('Rout');

        //Devuelta de los resultados de la segunda consulta.
        return $results;
    }

    public function getRoutInfo($id)
    {

        $this->db->query('SELECT * FROM rout inner join routdetails on rout.id_rout = routdetails.rout_id where rout.id_rout=:id');
        $this->db->bind(':id', $id);
        $result = $this->db->single('Rout');

        return $result;
    }

    /*---------------------------
    -       ACTUALIZAR RUTAS        -
    ----------------------------*/

    public function updateRoutes($data)
    {

        //Query para actualizar los campos de la ruta
        $this->db->query("UPDATE rout SET inicio=:inicio,destino=:destino,plazas=:plazas,gastos=:gastos,foto=:image_new where id_rout=:id");
        //Bindeo de los campos a cambiar
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':inicio', $data['inicio_new']);
        $this->db->bind(':destino', $data['destino_new']);
        $this->db->bind(':plazas', $data['plazas_new']);
        $this->db->bind(':gastos', $data['gastos_new']);
        $this->db->bind(':image_new', $data['image']);
        //Ejecución de la consulta
        $updated = $this->db->execute();

        //Comprobación de que se ha hecho la consulta
        if ($updated) {

            return true;
        } else {

            return false;
        }
    }

    /*---------------------------
    -   ACTUALIZAR Nº PLAZAS    -
    ----------------------------*/
    public function updatePlace($data)
    {

        //Query para actualizar el número de plazas de la ruta seleccionada
        $this->db->query("UPDATE rout SET plazas=:plazas WHERE id_rout=:id");
        //Bindeo de los datos a actualizar y obtención del id de la ruta a editar
        $this->db->bind(":plazas", $data['rout']->plazas - 1);
        $this->db->bind(':id', $data['rout']->id_rout);
        //Ejecución de la consulta
        $updated = $this->db->execute();

        //Comprobación de que la consulta se ha hecho
        if ($updated) {

            return true;
        } else {

            return false;
        }
    }

    /*----------------------------------------------------
    -   AÑADIR PLAZAS AL BORRAR A UN USUARIO DE UNA RUTA -
    ----------------------------------------------------*/
    public function addPlace($data)
    {
        //Query para actualizar el número de plazas de la ruta seleccionada
        $this->db->query("UPDATE rout SET plazas=:plazas WHERE id_rout=:id");
        //Bindeo de los datos a actualizar y obtención del id de la ruta a editar
        $this->db->bind(":plazas", $data['plazas']->plazas + 1);
        $this->db->bind(':id', $data['rout']->rout_id);
        //Ejecución de la consulta
        $updated = $this->db->execute();

        //Comprobación de que la consulta se ha hecho
        if ($updated) {

            return true;
        } else {

            return false;
        }
    }


    /*---------------------------
    -       OBTENER RUTA POR ID        -
    ----------------------------*/
    public function getRoutById($id)
    {

        //Query para la obtención de la ruta por id
        $this->db->query("SELECT * from rout where id_rout=:id");
        $this->db->bind(':id', $id);
        //Devuelta de un único valor de la consulta.
        $routes = $this->db->single('Rout');
        //Devuelta de valores
        return $routes;
    }

    /*---------------------------
    -       ELIMINAR RUTAS        -
    ----------------------------*/

    public function deleteRoutes($id)
    {

        //Query para eliminar la ruta seleccionada por id
        $this->db->query("DELETE FROM rout WHERE id_rout=:id;");
        $this->db->bind(':id', $id);
        //Ejecución de la consulta
        $deleted = $this->db->execute();

        //Devolvemos que la ruta se ha eliminado
        return $deleted;
    }

    /*-------------------------------
    - ELIMINAR USUARIO DE LA RUTA   -
    --------------------------------*/
    public function deleteUserRout($data)
    {

        $this->db->query("DELETE FROM routdetails where rout_users_id=:id AND rout_id=:idrout");
        $this->db->bind(':id', $data['rout']->rout_users_id);
        $this->db->bind(':idrout',$data['rout']->rout_id);
        $deleted = $this->db->execute();

        return $deleted;
    }

    /*----------------------------------------------
    - CHECKEAR USUARIO SI ESTÁ APUNTADO A UNA RUTA -
    ----------------------------------------------*/
    public function checkUserRout()
    {
        $this->db->query("SELECT * from routdetails");
        $checked = $this->db->single('Rout');
        return $checked;

        
    }
}
