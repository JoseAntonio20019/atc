<?php

class FileException extends Exception

{

    public function __construct(string $message)
    {

        //La variable parent se utiliza para llamar a la clase Padre 
        //de la que extienda la clase
        parent::__construct($message);
    }
}


?>