<nav class="navbar navbar-expand-lg navbar-light bg-dark p-3 ">
    <a class="navbar-brand text-white">ATC</a>
    <a class="nav-link text-white<?= (estaActiva('index') ? 'active' : '') ?>" href="<?= URLROOT . '/paginas/index' ?>">Inicio</a>
    <?php if (isLoggedIn()) {
        echo "
    <li class='nav-item active ms-auto d-flex'>
    <p class='text-white mt-2'>Usuario: " . $_SESSION['user_name'] . "</p>
                    <a class='nav-link " . (estaActiva('logout') ? 'active' : '') . "text-white' href=" . URLROOT . '/users/logout' . ">Cerrar Sesión</a>
    </li>
 ";
    } else {

        echo "
            
        
                <ul class='ms-auto d-flex'>    
                    <li class='nav-item'>
                    <a class='nav-link text-white" . (estaActiva('register') ? 'active' : '') . " ' href=" . URLROOT . '/users/register' . ">Registrarse</a>
                </li>

                <li class='nav-item'>
                    <a class='nav-link text-white" . (estaActiva('login') ? 'active' : '') . " ' href=" . URLROOT . '/users/login' . ">Iniciar Sesión</a>
                </li>";
    }
    ?>
    </ul>
</nav>