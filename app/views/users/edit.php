<?php require APPROOT . '/views/partials/header.php';
require APPROOT . '/views/partials/navbar.php';
?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2>Editar Usuario</h2>
            <form method="POST" action="<?= URLROOT . '/users/edit/'.$_SESSION['user_id'] ?>">
                <div class="form-group">
                    <label for="name">Nombre Nuevo: <sup>*</sup></label>
                    <input type="text" name="name_new" class="form-control <?php if (isset($data['name_err'])) {
                                                                            echo 'is-invalid';
                                                                        }
                                                                        if ($data['name_new'] != '') {
                                                                            echo 'is-valid';
                                                                        } ?>" value="<?= $data['name_old'] ?>">
                    <?php
                    if (isset($data['name_err'])) {

                        echo '<p class="text-danger">' . $data['name_err'] . '</p>';
                    } else if ($data['name_new'] != '') {

                        echo ('<p class="text-success">Nombre nuevo introducido válido!</p>');
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="email">Email Nuevo: <sup>*</sup></label>
                    <input type="email" name="email_new" class="form-control <?php if (isset($data['email_err']) && isset($data['check'])) {
                                                                                echo 'is-invalid';
                                                                            } else if (($data['email_new']) != '') {
                                                                                echo 'is-valid';
                                                                            }
                                                                            ?>" value="<?= $data['email_old'] ?>">
                    <?php
                    if (isset($data['email_err'])) {

                        echo '<p class="text-danger">' . $data['email_err'] . '</p>';
                    } else if ($data['email_new'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña Nueva: <sup>*</sup></label>
                    <input type="password" name="password_new" class="form-control <?php if (isset($data['password_err'])) {
                                                                                    echo "is-invalid";
                                                                                }
                                                                                if ($data['password_new'] != '') {
                                                                                    echo 'is-valid';
                                                                                }  ?>" value="<?= $data['password_old'] ?>">
                    <?php
                    if (isset($data['password_err'])) {

                        echo ('<p class="text-danger">' . $data['password_err'] . '</p>');
                    } else if ($data['password_new'] != '') {

                        echo ('<p class="text-success">Contraseña nueva introducida válida!</p>');
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="confirm_password">Número de Contacto Nuevo: <sup>*</sup></label>
                    <input type="text" name="phone_new" class="form-control <?php if (isset($data['phone_err'])) echo "is-invalid"; ?>" value="<?= $data['phone_old'] ?>">
                    <?php
                    if (isset($data['phone_err'])) {

                        echo ('<p class="text-danger">' . $data['phone_err'] . '</p>');
                    } else if ($data['phone_new'] != '') {

                        echo ('<p class="text-success">Teléfono nuevo introducido válido!</p>');
                    }
                    ?>
                </div>
                    <div class="col mt-3">
                        <input type="submit" value="Editar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
</body>
<footer class="text-center text-white bg-secondary fixed-bottom bg-dark p-5">
  <p>© 2021 Copyright:Jose Antonio Martínez Crespo  Versión:<?= APP_VERSION ?></p>
</footer>

</html>