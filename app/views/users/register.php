<?php require APPROOT . '/views/partials/header.php';
require APPROOT . '/views/partials/navbar.php';
?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2> Crear una cuenta</h2>
            <p>Por favor llena los campos para poder registrarse</p>
            <form method="POST" action="<?= URLROOT . '/users/register' ?>">
                <div class="form-group">
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?php if (isset($data['name_err'])) {
                                                                            echo 'is-invalid';
                                                                        }
                                                                        if ($data['name'] != '') {
                                                                            echo 'is-valid';
                                                                        } ?>" value="<?= $data['name'] ?>">
                    <?php
                    if (isset($data['name_err'])) {

                        echo '<p class="text-danger">' . $data['name_err'] . '</p>';
                    } else if ($data['name'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if (isset($data['email_err']) && isset($data['check'])) {
                                                                                echo 'is-invalid';
                                                                            } else if (($data['email']) != '') {
                                                                                echo 'is-valid';
                                                                            }
                                                                            ?>" value="<?= $data['email'] ?>">
                    <?php
                    if (isset($data['email_err'])) {

                        echo '<p class="text-danger">' . $data['email_err'] . '</p>';
                    } else if ($data['email'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if (isset($data['password_err'])) {
                                                                                    echo "is-invalid";
                                                                                }
                                                                                if ($data['password'] != '') {
                                                                                    echo 'is-valid';
                                                                                }  ?>" value="<?= $data['password'] ?>">
                    <?php
                    if (isset($data['password_err'])) {

                        echo ('<p class="text-danger">' . $data['password_err'] . '</p>');
                    } else if ($data['password'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?php if (isset($data['confirm_password_err'])) echo "is-invalid"; ?>" value="<?= $data['confirm_password'] ?>">
                    <?php
                    if (isset($data['confirm_password_err'])) {

                        echo ('<p class="text-danger">' . $data['confirm_password_err'] . '</p>');
                    } else if ($data['confirm_password'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="confirm_password">Número de Contacto: <sup>*</sup></label>
                    <input type="text" name="phone" class="form-control <?php if (isset($data['phone_err'])) echo "is-invalid"; ?>" value="<?= $data['phone'] ?>">
                    <?php
                    if (isset($data['phone_err'])) {

                        echo ('<p class="text-danger">' . $data['phone_err'] . '</p>');
                    } else if ($data['phone'] != '') {

                        echo ('<p class="text-success">Valor introducido válido!</p>');
                    }
                    ?>
                </div>


                <div class="row">
                    <div class="col">
                        <a href="<?= URLROOT . '/users/login' ?>">¿Ya tienes cuenta? Inicia sesión</a>
                    </div>
                    <div class=" col">
                        <input type="submit" value="Registrar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>