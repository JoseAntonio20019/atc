<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right mt-4 ms-4" href="<?= URLROOT . '/routes/showedit/' . $_SESSION['user_id'] ?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i>
</a>
<div class="col-md-6 mb-5 mx-auto card bg-light">
    <div class="card-body ">
    <h2>Editar Ruta</h2>
    <p>Introduzca los datos que quiera actualizar para la ruta.</p>
    <form enctype="multipart/form-data" method="POST" action="<?= URLROOT . '/routes/edit/' . $data['id'] ?>">
        <div class="form-group">
            <label for="inicio_new_new">Nuevo Inicio: <sup>*</sup></label>
            <input type="text" name="inicio_new" class="form-control <?php if (isset($data['inicio_err'])) {
                                                                            echo "is-invalid";
                                                                        }
                                                                        if ($data['inicio_new'] != '' || $data['inicio_err'] == '') {
                                                                            echo 'is-valid';
                                                                        } ?>" placeholder="Indicación de su ruta nueva" value="<?= $data['inicio_old'] ?>">
            <?php
            if (isset($data['inicio_err'])) {

                echo '<p class="text-danger">' . $data['inicio_err'] . '</p>';
            } else if ($data['inicio_new'] != '') {

                echo '<p class="text-success">Inicio nuevo introducido correctamente.</p>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="destino_new">Nuevo Destino: <sup>*</sup></label>
            <input type="text" name="destino_new" class="form-control <?php if (isset($data['destino_err'])) {
                                                                            echo "is-invalid";
                                                                        }
                                                                        if ($data['destino_new'] != '' || $data['destino_err'] == '') {
                                                                            echo 'is-valid';
                                                                        } ?>" placeholder="Indicación de su destino nuevo" value="<?= $data['destino_old'] ?>">
            <?php
            if (isset($data['destino_err'])) {

                echo '<p class="text-danger">' . $data['destino_err'] . '</p>';
            } else if ($data['destino_new'] != '') {

                echo '<p class="text-success">Destino nuevo introducido correctamente.</p>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="plazas_new">Plazas: <sup>*</sup></label>
            <input type="text" name="plazas_new" class="form-control <?php if (isset($data['plazas_err'])) {
                                                                            echo "is-invalid";
                                                                        }
                                                                        if ($data['plazas_new'] != '' || $data['plazas_err'] == '') {
                                                                            echo 'is-valid';
                                                                        } ?>" placeholder="Indicación del número de plazas del vehículo nuevo" value="<?= $data['plazas_old'] ?>">
            <?php
            if (isset($data['plazas_err'])) {

                echo '<p class="text-danger">' . $data['plazas_err'] . '</p>';
            } else if ($data['plazas_new'] != '') {

                echo '<p class="text-success">Nº Plazas nuevas introducidas correctamente.</p>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="gastos_new">Precio/Persona: <sup>*</sup></label>
            <input type="text" name="gastos_new" class="form-control <?php if (isset($data['gastos_err'])) {
                                                                            echo "is-invalid";
                                                                        }
                                                                        if ($data['gastos_new'] != '' || $data['gastos_err'] == '') {
                                                                            echo 'is-valid';
                                                                        } ?>" placeholder="Gastos del viaje(Gasolina,etc)" value="<?= $data['gastos_old'] ?>">
            <?php
            if (isset($data['gastos_err'])) {

                echo '<p class="text-danger">' . $data['gastos_err'] . '</p>';
            } else if ($data['gastos_new'] != '') {

                echo '<p class="text-success">Gastos introducidos correctamente.</p>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="file">Nueva Imagen del Trayecto(Opcional):</label>
            <img src="<?php 
                    if(empty($data['image_old'])){

                        echo URLROOT . '/public/img/ver.jpg';
                    }else{
                        echo URLROOT . '/public/img/' . $data['image_old'];
                    }
                ?>" width="200"</img>
                <input type="hidden" name="image_old">
        <input type="file" name="image" class="form-control <?php if (isset($data['image_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ( $data['image_err']=='') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="" value="">
             <?php
                    if (isset($data['image_err'])) {

                        echo '<p class="text-danger">' . $data['image_err'] . '</p>';
                    } else if ($data['image'] != '') {

                        echo '<p class="text-success">Imagen introducida correctamente.</p>';
                    }
                    ?>

        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Editar Ruta" class="btn btn-success btn-block">
            </div>
        </div>
    </form>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
</body>
<footer class="text-center text-white bg-secondary position-sticky bg-dark p-5">
  <p>© 2021 Copyright:Jose Antonio Martínez Crespo  Versión:<?= APP_VERSION ?></p>
</footer>

</html>