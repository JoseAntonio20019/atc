<?php

require APPROOT . '/views/partials/header.php';
require APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right mt-4 ms-4" href="<?= URLROOT . '/routes/index/' ?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i>
</a>
<div class="flashes">

    <?= (string) flash() ?>
</div>

<h3 class="text-center">Editor de Rutas</h3>
<div class="container mt-5 p-3">
    <div class="row row-cols">
        <div class="row">
            <?php

            if (empty($data['rout'])) {
            ?>
                <h3>No tienes rutas creadas este momento. Tómate un respiro.</h3>
                <?php
            } else {
                foreach ($data['rout'] as $rout) {
                ?>
                    <div class="card p-3 border bg-light w-75">
                        <div class="card-body">

                            <h4><?= $rout->inicio ?><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                </svg><?= $rout->destino ?></p>
                            </h4>
                            <p class="card-text">Creada por: <?= $data['user']->name ?> el <?= $data['user']->created_at ?></p>
                            <p class="card-text">Nº Plazas:<?= $rout->plazas ?></p>
                            <p class="card-text">Precio/Persona:<?= $rout->gastos ?>€</p>

                    <?php
                    if ($_SESSION['user_id'] == $data['user']->id) {

                        echo '
                        <div class="d-flex justify-content-evenly">
                    <a href=' . URLROOT . "/routes/edit/" . $rout->id_rout . ' class="btn btn-success btn-block">
                        <i class="fas fa-edit">Editar</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill ms-2 ms-2" viewBox="0 0 16 16">
                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                      </svg>
                    </a>
                    <a href=' . URLROOT . '/routes/details/' . $rout->id_rout . ' class="btn btn-primary btn-block">
                        <i class="fas fa-edit">Ver usuarios de la ruta</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill ms-2" viewBox="0 0 16 16">
                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
                        <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                      </svg> 
                    </a>
                            
                    <form action=' . URLROOT . "/routes/delete/" . $rout->id_rout . ' method="post">                        
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash">Borrar post</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill ms-2 ms-2" viewBox="0 0 16 16">
                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                          </svg>
                        </button>
                    </form>
                    </div>
                    </div>
            </div>
            ';
                    }
                }
            }
                    ?>
                        </div>
                    </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
        </body>
        <footer class="text-center text-white bg-secondary fixed-bottom bg-dark p-5">
            <p>© 2021 Copyright:Jose Antonio Martínez Crespo</p>
        </footer>

        </html>