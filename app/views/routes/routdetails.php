<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';


?>
<div class="flashes">

    <?= (string) flash() ?>
</div>
<a class="btn btn-warning pull-right mt-4 ms-4" href="<?= URLROOT . '/routes/index' ?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i>
</a>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <div class="row-md-6 mt-1">
        <h2 class="text-center">TUS RUTAS</h2>
    </div>
    <?php


     if (empty($data['rout'])) {
    ?>
        <h3>No hay rutas a las que estés apuntado ahora mismo.</h3>
        <?php
    } else { 
        foreach ($data['rout'] as $rout) {
                

        ?>
            <div class="card mb-4 mt-4">
                <div class="card-body">

                    <h4><?= $rout->inicio ?><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg><?= $rout->destino ?></p>
                    </h4>

                    <img src="<?php
                                if (empty($rout->foto)) {

                                    echo URLROOT . '/public/img/ver.jpg';
                                } else {
                                    echo URLROOT . '/public/img/' . $rout->foto;
                                }
                                ?>" width="300"></img>
                    <p class="card-text mt-3">Dueño de la ruta:<?= $rout->name ?></p>
                    <p class="card-text">Precio/Persona: <?= $rout->gastos ?>€</p>
                    <p class="card-text">Email Contacto:<?= $rout->email ?></p>
                    <p class="card-text">Número Contacto:<?= $rout->phone ?></p>
                    <form action='<?= URLROOT . "/routes/deleteUserRoutShow/" . $_SESSION['user_id'] ?>' method="post">                        
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash">Salirse de la ruta</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill ms-2 ms-2" viewBox="0 0 16 16">
                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                          </svg>
                        </button>
                    </form>

                </div>
            </div>

        


    <?php
        }
    }


    ?>

</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
    </body>
    <footer class="text-center text-white bg-secondary position-sticky bg-dark p-5">
        <p>© 2021 Copyright:Jose Antonio Martínez Crespo</p>
    </footer>

    </html>