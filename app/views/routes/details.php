<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right mt-4 ms-4 mb-4" href="<?= URLROOT . '/routes/showedit/' . $_SESSION['user_id'] ?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i>
</a>
<div class="flashes">

    <?= (string) flash() ?>
</div>
<h3 class="text-center mt-4">Usuarios de la Ruta:</h3>
<div class="container mt-5">
    <div class="row row-cols">
        <div class="row ">
            <?php

            if (empty($data)) {

            ?>
                <h4 class="text-center"> No hay usuarios añadidos a tu ruta.</h4>

            <?php
            }
            foreach ($data as $user) {

            ?>

                <div class="card p-3 border bg-light w-50">
                    <div class="card-body">
                        <h5>Nombre:</h5>
                        <p><?= $user->name ?></p>
                        <h5>Email:</h5>
                        <p> <?= $user->email ?></p>
                        <h5>Número Contacto:</h5>
                        <p><?= $user->phone ?></p>
                    </div>
                    <form action='<?= URLROOT . "/routes/deleteUserRout/" . $user->id ?>' method="post">                        
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash">Borrar usuario</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill ms-2 ms-2" viewBox="0 0 16 16">
                            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                          </svg>
                        </button>
                    </form>
                </div>


            <?php
            }
            ?>

        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
</body>
<footer class="text-center text-white bg-secondary position-sticky bg-dark p-5">
    <p>© 2021 Copyright:Jose Antonio Martínez Crespo</p>
</footer>

</html>