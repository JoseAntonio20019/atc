<?php

require APPROOT . '/views/partials/header.php';
require APPROOT . '/views/partials/navbar.php';
?>

<div class="align-items-center">
   <h3 class=" ms-6 mt-4">Bienvenido de nuevo: <?= $_SESSION['user_name'] ?></h3>
</div>

<div class="col-lg-8 mx-auto p-3 py-md-5 d-flex justify-content-around gap-5">
   <div class="col btn btn-success" role="button">
      <a class="text-white text-decoration-none" href="<?= URLROOT . '/routes/add' ?>">
         <img style="border: 5px;" src="<?= URLROOT . '/public/img/crear.png' ?>" width="500" height="400"></img>
         Crear rutas<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-circle ms-2" viewBox="0 0 16 16">
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
         </svg>
      </a>
   </div>
   <div class="col btn btn-warning" role="button">
      <a class="text-white text-decoration-none" href="<?= URLROOT . '/routes/showedit/' . $_SESSION['user_id'] ?>">
         <img style="border: 5px;" src="<?= URLROOT . '/public/img/editar.jpg' ?>" width="500" height="400"></img>Editar tus rutas<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-gear ms-2" viewBox="0 0 16 16">

            <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z" />
            <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z" />
         </svg>
      </a>
   </div>
   <div class="col btn btn-primary" role="button">
      <a class="text-white text-decoration-none" href="<?= URLROOT . '/routes/show' ?>">
         <img style="border: 5px;" src="<?= URLROOT . '/public/img/ver.jpg' ?>" width="500" height="400"></img>Mostrar todas las rutas<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-search ms-2" viewBox="0 0 16 16">

            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
         </svg>
      </a>
   </div>
</div>
<div class="container d-flex text-center justify-content-evenly">
   <div class="row btn btn-info ms-6" role="button">
      <a class="text-white text-decoration-none" href="<?= URLROOT . '/routes/routdetails/' . $_SESSION['user_id'] ?>">
         Mostrar mis rutas
         <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
         </svg>
      </a>

   </div>
   <div class="row btn btn-warning ms-6" role="button">
      <a class="text-white text-decoration-none" href="<?= URLROOT . '/users/edit/' . $_SESSION['user_id'] ?>">
         Editar Usuario
         <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
         </svg>
      </a>
   </div>

   <form action='<?= URLROOT . "/users/deleteUser/" . $_SESSION['user_id'] ?>' method="post">
      <button type="submit" class="btn btn-danger btn-block">
         <i class="fas fa-trash">Eliminar Usuario</i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill ms-2 ms-2" viewBox="0 0 16 16">
            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
         </svg>
      </button>
   </form>
</div>


<?php require APPROOT . '/views/partials/footer.php'; ?>