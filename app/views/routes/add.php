<?php

    require_once APPROOT . '/views/partials/header.php';
    require_once APPROOT . '/views/partials/navbar.php';

?>
<a class="btn btn-warning pull-right mt-4 ms-4" href="<?= URLROOT . '/routes/index'?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i> 
</a>
<div class="col-md-6 mx-auto card bg-light p-3">
    <h2>Crear Ruta</h2>
    <p>Introduzca los datos necesarios para crear la ruta</p>
    <form enctype="multipart/form-data" method="POST" action="<?= URLROOT . '/routes/add' ?>">
        <div class="form-group">
            <label for="inicio">Inicio: <sup>*</sup></label>
            <input type="text" name="inicio" class="form-control <?php if (isset($data['inicio_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['inicio'] != '' || $data['inicio_err'] == '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Indicación de su ruta" value="<?= $data['inicio'] ?>">
             <?php
                    if (isset($data['inicio_err'])) {

                        echo '<p class="text-danger">' . $data['inicio_err'] . '</p>';
                    } else if ($data['inicio'] != '') {

                        echo '<p class="text-success">Inicio introducido correcto.</p>';
                    }
                    ?>
        </div>
        <div class="form-group">
            <label for="destino">Destino: <sup>*</sup></label>
            <input type="text" name="destino" class="form-control <?php if (isset($data['destino_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['destino'] != '' || $data['destino_err'] == '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Indicación de su destino" value="<?= $data['destino'] ?>">
             <?php
                    if (isset($data['destino_err'])) {

                        echo '<p class="text-danger">' . $data['destino_err'] . '</p>';
                    } else if ($data['destino'] != '') {

                        echo '<p class="text-success">Destino introducido correcto.</p>';
                    }
                    ?>
        </div>
        <div class="form-group">
            <label for="plazas">Plazas: <sup>*</sup></label>
            <input type="text" name="plazas" class="form-control <?php if (isset($data['plazas_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['plazas'] != '' || $data['plazas_err'] == '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Indicación del número de plazas del vehículo" value="<?= $data['plazas'] ?>">
             <?php
                    if (isset($data['plazas_err'])) {

                        echo '<p class="text-danger">' . $data['plazas_err'] . '</p>';
                    } else if ($data['plazas'] != '') {

                        echo '<p class="text-success">Nº Plazas introducidas correctamente.</p>';
                    }
                    ?>
        </div>
        <div class="form-group">
            <label for="gastos">Gastos: <sup>*</sup></label>
            <input type="text" name="gastos" class="form-control <?php if (isset($data['gastos_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['gastos'] != '' || $data['gastos_err'] == '') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="Gastos del viaje(Gasolina,etc)" value="<?= $data['gastos'] ?>">
             <?php
                    if (isset($data['gastos_err'])) {

                        echo '<p class="text-danger">' . $data['gastos_err'] . '</p>';
                    } else if ($data['gastos'] != '') {

                        echo '<p class="text-success">Gastos introducidos correctamente.</p>';
                    }
                    ?>
        </div>
        
        <div class="form-group">
            <label for="file">Añadir Imagen del Trayecto(Opcional):</label>
        <input type="file" name="image" class="form-control <?php if (isset($data['image_err'])) {
                                                                                echo "is-invalid";
                                                                            }
                                                                            if ($data['image'] || $data['image_err']=='') {
                                                                                echo 'is-valid';
                                                                            } ?>" placeholder="" value="">
             <?php
                    if (isset($data['image_err'])) {

                        echo '<p class="text-danger">' . $data['image_err'] . '</p>';
                    } else if ($data['image'] != '') {

                        echo '<p class="text-success">Imagen introducida correctamente.</p>';
                    }
                    ?>

        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php

    require_once APPROOT . '/views/partials/footer.php';

?>