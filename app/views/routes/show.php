<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';


?>
<div class="flashes">

    <?= (string) flash() ?>
</div>
<a class="btn btn-warning pull-right mt-4 ms-4" href="<?= URLROOT . '/routes/index' ?>" role="button">
    <i class="fas fa-arrow-left">Regresar</i>
</a>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <div class="row-md-6 mt-1">
        <h2 class="text-center">RUTAS</h2>
    </div>


    <?php
    if (empty($data['routes'])) {
    ?>
        <h3>No hay rutas creadas en este momento. Vuelva más tarde, o puede crear una clickando <a href="<?= URLROOT . '/routes/add' ?>">aquí</a></h3>
        <?php
    } else {
        foreach ($data['routes'] as $rout) {
        ?>
            <div class="card mb-4 mt-4">
                <div class="card-body">

                    <h4><?= $rout->inicio ?><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg><?= $rout->destino?></p>
                    </h4>

                    <img src="<?php
                                if (empty($rout->foto)) {

                                    echo URLROOT . '/public/img/ver.jpg';
                                } else {
                                    echo URLROOT . '/public/img/' . $rout->foto;
                                }
                                ?>" width="300"></img>
                    <p class="card-text mt-2">Creada por: <?= $rout->name ?> el <?= $rout->created_at ?></p>
                    <p class="card-text">Nº Plazas Disponibles: <?= $rout->plazas ?></p>
                    <p class="card-text">Precio/Persona: <?= $rout->gastos ?>€</p>
                    <p class="card-text">Número Contacto: <?= $rout->phone ?></p>
                    <p class="text-muted mt-3">*Si te has apuntado ya a una ruta y la quieres cancelar,dirigete al apartado de tus rutas y salte de ella.*</p>



                <?php

                if ($_SESSION['user_id'] == $rout->users_id) {

                    echo '
                        <h6 class="text-center mb-5">No puedes apuntarte a una ruta que has creado</h6>
                </div>
            </div>';
                }else{
                
                    echo '<a class="btn btn-success" role="button" href=' . URLROOT . "/routes/check/" . $rout->id_rout . '>Apuntarse a la ruta</a>
                    </div>
            </div>';
                }
            }
            }
                ?>



</div>




</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
</body>
<footer class="text-center text-white bg-secondary position-sticky bg-dark p-5">
    <p>© 2021 Copyright:Jose Antonio Martínez Crespo</p>
</footer>

</html>