<?php

require_once APPROOT . '/views/partials/header.php';
require_once APPROOT . '/views/partials/navbar.php';


if (isLoggedIn()) {

    redirect('/routes/index');
} else {
?>

    <div class="col-lg-8 mx-auto p-3 py-md-5">

        <main>
            <h1>Bienvenido a Alquilamos tu Coche</h1>
            <p class="fs-5 col-md-8">La web en la que puedes acceder a infinidad de trayectos y por un coste mínimo.¿Quieres saber cómo? Clicka en el botón de abajo para registrarte</p>

            <div class="mb-5">
                <a href="<?= URLROOT . '/users/register' ?>" class="btn btn-primary btn-lg px-4">Registrate</a>
            </div>

            <hr class="col-3 col-md-2 mb-5">
            <div class="row g-5">
                <div class="col-md-6">
                    <h2>Utilidades de la página</h2>
                    <p>Contamos con la mayor variedad de rutas flexibles y baratas del sector.Si ya eres usuario puedes iniciar sesión en el botón de abajo.</p>
                    <a href="<?= URLROOT . '/users/login' ?>" class="btn btn-primary btn-lg px-4">Inicia Sesión</a>
                </div>

                <div class="col-md-6">
                    <h2>Quiénes Somos</h2>
                    <p>Somos una pequeña empresa situada en Fuerteventura con el propósito de facilitar a más gente el poder desplazarse por la isla, aunque no nos negamos a hacer la app a nivel nacional</p>
                </div>
        </main>
    </div>

    </div>
<?php
}
require_once APPROOT . '/views/partials/footer.php';

?>