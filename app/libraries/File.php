<?php

require_once APPROOT . '/exceptions/FileException.php';


class File extends FileException
{

    private $nombrearchivo;
    private $data;

    public function __construct($nombrearchivo, $data)
    {

        $this->nombrearchivo = $nombrearchivo;
        $this->data = $data;
    }

    public function checker()
    {

        //$_FILES es un array que contiene varios métodos POST a la hora de insertar archivos en un formulario POST
        if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {

            switch ($_FILES['image']['error']) {

                case UPLOAD_ERR_INI_SIZE: //Este errror nos indica que se ha excedido el tamaño máximo de la variable upload_max_filesize del archivo php.ini
                case UPLOAD_ERR_FORM_SIZE: //Este error nos indica que se ha excedido el tamaño máximo del formulario
                    throw new FileException('El fichero es demasiado grande. Tamaño máximo:'
                        . ini_get('upload_max_filesize'));
                    //ini_get es una funcion que hace un get a variables dentro del archivo php.ini
                    break;
                case UPLOAD_ERR_PARTIAL: //Este error nos indica que el archivo no se ha subido entero.
                    throw new FileException('No se ha podido subir el fichero entero');
                    break;
                default:
                    throw new FileException('Error al subir el fichero');
                    break;
            }
        }
    }

    public function saveUploadFile($ruta){

        if(move_uploaded_file($_FILES['image']['tmp_name'], $ruta . $_FILES['image']['name']) === false){

            throw new FileException('Error al subir el fichero');

        }else{

            return true;
        }

    
    }
}