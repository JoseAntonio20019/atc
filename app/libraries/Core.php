<?php

class Core
{

    protected $controladorActual = "Paginas"; //Parámetro que será nuestro controlador por defecto.

    protected $metodoActual = "index"; //Parámetro con el método por defecto.

    protected $parametros = []; // Array que devolverá los parámetros que se vayan introduciendo en la url

    public function __construct() //Creación del constructor
    {


        $url = $this->getURL(); //Llamada al método GETURL();

        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) { //Comprobamos si existe el archivo mediante una creacion dentro de file_exists, y cambiamos a mayusculas para comprobar correctamente si existe el archivo o no.


            $this->controladorActual = ucwords($url[0]); //Ponemos a mayúsculas la url;
            unset($url[0]); //Eliminamos de url con la función unset.
        }

        require_once '../app/controllers/' . $this->controladorActual . '.php';
         // echo $this->controladorActual; // Hacemos un require de controllers app.
        $this->controladorActual =new $this->controladorActual; // Creamos un nuevo controlador de nuestro propio controlador.


        if (isset($url[1])) {

            if (method_exists($this->controladorActual, $url[1])) {

                $this->metodoActual = $url[1];

                unset($url[1]);
            }
        }

         //echo $this->metodoActual;
        //echo $this->controladorActual; */

        $this->parametros = $url ? array_values($url) : []; //Comprueba si existen parametros en la url, y crea un array vacío en caso de que exista

        call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros); //Guarda en un array la url entera, y la devuelve separada por parámetros
    }

    //Pregunta call_user_func_array
    //Porque si hacemos directamente la obtención de la url sin call_user_func_array no se separa por controlador, metodo y parametros, da todo junto, en cambio la función te devuelve el array con los parámetros separados




    public function getURL() //Creación del método url

    {


        //echo $_GET['url']; Se hace un $_GET para obtener la url de la página.


        if (isset($_GET['url'])) { //Comprobamos si existe la variable url.

            $url = rtrim($_GET['url'], '/'); //Eliminamos los espacios en blanco y los separamos por una barra.
            $url = filter_var($url, FILTER_SANITIZE_URL); //Sanitizamos la url para no dejar libre la inyección html por url.
            $url = explode('/', $url); //Dividimos por barras todos los espacios en la url.  



        } else {

            $url[0] = "Paginas";
        }

        return $url;
    }
}
