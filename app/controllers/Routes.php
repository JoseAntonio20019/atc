<?php


//Uso de la libería SimpleFlash de composer
use Tamtamchik\SimpleFlash\Flash;

class Routes extends Controller
{


    //Constructor de Routes
    public function __construct()
    {

        //Checkeo de Usuario Logueado
        if (!isLoggedIn()) {

            redirect('users/login');
        }

        $this->routModel = $this->model("Rout");
        $this->userModel = $this->model("User");
    }

    /*--------------
    -     INDEX    -
    --------------*/
    public function index()
    {

        $this->view('routes/index');
    }

    /*--------------
    -     ADD    -
    --------------*/

    public function add()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            //Recibimoms los datos por POST
            $data['inicio'] = trim($_POST['inicio'], " ");
            $data['destino'] = trim($_POST['destino'], " ");
            $data['plazas'] = trim($_POST['plazas'], " ");
            $data['gastos'] = trim($_POST['gastos'], " ");
            $data['image'] = $_FILES['image']['name'];
            $data['user_id'] = $_SESSION['user_id'];

            //Sanitizamos los valores del array
            filter_input_array(INPUT_POST, $data);

            //Comprobación de errores

            if ($data['inicio'] == '') {

                $data['inicio_err'] = "Inicio no introducido. Introduzca uno";
            }

            if ($data['destino'] == '') {

                $data['destino_err'] = "Destino no introducido. Introduzca uno";
            }

            if ($data['plazas'] == '') {

                $data['plazas_err'] = "Plazas no introducidas. Introduzca un número";
            }

            if ($data['gastos'] == '') {

                $data['gastos_err'] = "Gastos no añadidos. Introduzca los gastos del viaje";
            }


            //Añadir Imagen

            if (!empty($data['image'])) {

                try {

                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    $file = new File($_FILES['image'], $arrTypes);
                    $file->checker();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {

                    $data['image_err'] = $error->getMessage();
                }
            }

            //Comprobación de que no existen errores
            if (isset($data['inicio_err']) || isset($data['destino_err']) || isset($data['plazas_err']) || isset($data['gastos_err'])) {

                $this->view('routes/add', $data);
            } else {

                //Llamamos al modelo y creamos una ruta con los valores de $data
                $insertar = $this->routModel->addRoutes($data);

                //Comprobamos que se inserte, y si es así creamos un mensaje flash y un redirect.
                if ($insertar) {

                    //Mensaje flash creado,enviado y un redirect a la vista
                    $flash = new Flash();

                    $flash->message('Ruta creada correctamente', 'info');

                    redirect('/routes/show');
                }
            };
        } else {

            //Inicializar $data con valores por defecto
            $data = [

                'user_id' => '',
                'inicio' => '',
                'inicio_err' => '',
                'destino' => '',
                'destino_err' => '',
                'plazas' => '',
                'plazas_err' => '',
                'gastos' => '',
                'gastos_err' => '',
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'image_err' => ''
            ];
            $this->view('routes/add', $data);
        }
    }


    /*--------------
    -     DELETE    -
    --------------*/
    function delete($id)
    {

        //Comprobación de si es por POST la petición 
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            //Eliminación de ruta por id llamando al modelo
            $this->routModel->deleteRoutes($id);
            //Mensaje flash y redirect
            $flash = new Flash();
            $flash->message('Ruta eliminada con éxito', 'info');
            redirect('/routes/showedit/' . $_SESSION['user_id']);
        }
    }


    /*--------------
    -     EDIT    -
    --------------*/
    function edit($id)
    {

        //Comprobación de si es por POST la petición
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data['image_old'] = trim($_POST['image_old'], " ");
            $data['id'] = $id;
            $data['inicio_new'] = trim($_POST['inicio_new'], " ");
            $data['destino_new'] = trim($_POST['destino_new'], " ");
            $data['plazas_new'] = trim($_POST['plazas_new'], " ");
            $data['gastos_new'] = trim($_POST['gastos_new'], " ");
            $data['image'] = $_FILES['image']['name'];

            //Sanitización del array $data
            filter_input_array(INPUT_POST, $data);

            //Comprobación de errores

            if ($data['inicio_new'] == '') {

                $data['inicio_err'] = "Inicio nuevo vacío. Introduzca uno";
            }

            if ($data['destino_new'] == '') {

                $data['destino_err'] = "Destino nuevo vacío. Introduzca uno";
            }
            if ($data['plazas_new'] == '') {

                $data['plazas_err'] = "Destino nuevo vacío. Introduzca uno";
            }
            if ($data['gastos_new'] == '') {

                $data['gastos_err'] = "Destino nuevo vacío. Introduzca uno";
            }

            if (!empty($data['image'])) {

                try {

                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    $file = new File($_FILES['image'], $arrTypes);
                    $file->checker();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {

                    $data['image_err'] = $error->getMessage();
                }
            } else {
                $rout = $this->routModel->getRoutById($data['id']);
                $data['image'] = $rout->foto;
            }


            //Comprobación de que no existan errores
            if (isset($data['inicio_err']) || isset($data['destino_err']) || isset($data['plazas_err']) || isset($data['gastos_err']) || isset($data['image_err'])) {

                $this->view('routes/edit', $data);
            } else {

                //Actualizar ruta con los valores de $data llamando al modelo de ruta
                $updated = $this->routModel->updateRoutes($data);

                //Comprobación de que está actualizado
                if ($updated) {

                    //Mensaje flash y redirect 
                    $flash = new Flash();

                    $flash->message('Ruta actualizada correctamente', 'info');

                    redirect('/routes/showedit/' . $_SESSION['user_id']);
                }
            }
        } else {

            //Inicialización de $data a las variables y sus errores
            $data = [

                'inicio_new' => '',
                'inicio_err' => '',
                'destino_new' => '',
                'destino_err' => '',
                'plazas_new' => '',
                'plazas_err' => '',
                'gastos_new' => '',
                'gastos_err' => '',
                'image_new' => '',
                'image_err' => ''

            ];

            //Obtención de los datos de ruta para rellenar cada hueco con su valor.
            $routes = $this->routModel->getRoutById($id);

            $data['id'] = $id;
            $data['inicio_old'] = $routes->inicio;
            $data['destino_old'] = $routes->destino;
            $data['plazas_old'] = $routes->plazas;
            $data['gastos_old'] = $routes->gastos;
            $data['image_old'] = $routes->foto;
            $this->view('routes/edit', $data);
        }
    }

    /*--------------
    -    SHOW      - 
    --------------*/
    public function show()
    {
        //Get con todas las rutas disponibles para apuntarse
        $data['routes'] = $this->routModel->getRoutes();
        $this->view('routes/show', $data);
    }

    /*--------------
    -    SHOWEDIT  -
    --------------*/

    public function showedit($id)
    {

        //Obtener usuario y ruta mediante id, meter en $data y mandar a la vista
        $rout = $this->routModel->getRoutesByUserId($id);
        $user = $this->userModel->getUserById($id);
        //Bindeo de los gets en el array $data
        $data['rout'] = $rout;
        $data['user'] = $user;
        $this->view('routes/showedit', $data);
    }

    /*--------------
    -     CHECK    -
    --------------*/


    public function check($id)
    {
        //Querys para obtener la info del usuario y la ruta
        $rout = $this->routModel->getRoutById($id);
        $user = $this->userModel->getUserById($_SESSION['user_id']);
        $data['user'] = $user;
        $data['rout'] = $rout;

        $check = $this->routModel->checkUserRout();
        $data['check'] = $check;
        var_dump($data['check']);

        if ($data['rout']->plazas === 0) {

            $flash = new Flash();
            $flash->message('No hay plazas disponibles. Escoge otra', 'warning');
            redirect('routes/show');

        } else if ($data['check']->rout_users_id == $data['user']->id && $data['check']->rout_id == $data['rout']->id_rout) {

            $flash = new Flash();
            $flash->message('Ya estás en esta ruta, escoge otra', 'warning');
            redirect('/routes/show');
        } else{

                $this->routModel->updatePlace($data);
                $this->routModel->addUserRout($data);
                $flash = new Flash();
                $flash->message('Has entrado en la ruta.¡Gracias por apuntarte!');
                redirect('/routes/show');
            
        }
    }








    /*--------------
    -     DETAILS  -
    --------------*/
    public function details($id)
    {
        //Get con todos los datos de los usuarios de la ruta pasada por id
        $data = $this->routModel->getRoutDetails($id);
        $this->view('routes/details', $data);
    }


    /*--------------
    - ROUTDETAILS  -
    --------------*/

    public function routdetails($id)
    {

        //Get con la información relevante en la vista de las rutas a las que está apuntado el usuario
        $user = $this->routModel->getRoutesInfoById($id);
        $data['rout'] = $user;
        $this->view('routes/routdetails', $data);
    }

    /*----------------
    - DELETEUSERROUT -
    ----------------*/
    public function deleteUserRout($id)
    {
        //Query para llamar a la función que elimina el usuario introducido por id desde la vista del creador de la ruta
        $rout = $this->routModel->getRoutDetailsByUserId($id);
        //Bindeo de los datos en $data
        $data['rout'] = $rout;
        $data['plazas'] = $this->routModel->getRoutInfo($data['rout']->rout_id);
        //Eliminamos al usuario por id
        $this->routModel->deleteUserRout($id);
        //Actualizamos el nº de plazas al eliminarlo.
        $this->routModel->addPlace($data);
        //Mensaje flash y redirect
        $flash = new Flash();
        $flash->message('Usuario Eliminado correctamente.');

        redirect('/routes/details/' . $data['rout']->rout_id);
    }

    /*----------------
    - DELETEUSERROUTSHOW -
    ----------------*/
    public function deleteUserRoutShow($id)
    {
        //Query para llamar a la función que elimina el usuario introducido por id desde la vista del propio usuario
        $rout = $this->routModel->getRoutDetailsByUserId($id);
        $data['rout'] = $rout;
        $data['plazas'] = $this->routModel->getRoutInfo($data['rout']->rout_id);
        $this->routModel->deleteUserRout($data);
        $this->routModel->addPlace($data);
        $flash = new Flash();
        $flash->message('Te has eliminado de la ruta correctamente.');
        redirect('/routes/routdetails/' . $_SESSION['user_id']);
    }
}
