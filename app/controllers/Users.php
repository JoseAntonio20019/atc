<?php

// Uso de la libreria SimpleFlash de composer
use Tamtamchik\SimpleFlash\Flash;

class Users extends Controller
{

    //Constructor
    public function __construct()
    {

        //Ejecución de la función model del modelo Usuario
        $this->userModel = $this->model("User");
    }


    /*---------------------------
    -     REGISTRO USUARIO     -
    ----------------------------*/
    public function register()
    {

        //Comprobación de si el request es POST
        if ($_SERVER["REQUEST_METHOD"] == 'POST') {


            //Introducción de los archivos en el array $data
            $insertado = '';
            $data['name'] = trim($_POST['name'], " ");
            $data['email'] = trim($_POST['email'], " ");
            $data['password'] = trim($_POST['password'], " ");
            $data['confirm_password'] = trim($_POST['confirm_password'], " ");
            $data['phone'] = trim($_POST['phone'], " ");

            //Sanitización de los datos
            filter_input_array(INPUT_POST, $data);

            //Comprobación de errores
            if ($data['name'] == '') {

                $data['name_err'] = "Introduce un nombre";
            }

            if ($data['email'] == '') {

                $data['email_err'] = "Introduce un correo electrónico";
            }

            //Checkeo de que el usuario no está ya vinculado a otro usuario
            $data['check'] = $this->userModel->findUserByEmail($data['email']);

            if ($data['check']) {

                $data['email_err'] = "El correo electrónico introducido pertenece a una cuenta existente. Introduzca otro. ";
            }

            if ($data['password'] == '') {

                $data['password_err'] = "Introduce una contraseña";
            } else if (strlen($data['password']) < 5) {

                $data['password_err'] = "La contraseña es menor a 6 caracteres. Escriba una con mayor longitud.";
            }

            if ($data['confirm_password'] == '') {

                $data['confirm_password_err'] = "Introduce una contraseña de confirmación";
            }


            //Comprobación de que ambas contraseñas son idénticas
            if ($data['confirm_password'] != $_POST['password']) {


                $data['confirm_password_err'] = "Las contraseñas no coinciden.";
            } else {

                //Hasheo a la variable $data['password] para encriptarla
                $data['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
            }

            if ($data['phone'] == '') {


                $data['phone_err'] = "Número de contacto no introducido, introduzca uno.";;
            } else if (strlen($data['phone']) != 9) {

                $data['phone_err'] = "Número de contacto inválido";
            }

            //Comprobación de si hay errores
            if (isset($data['name_err']) || isset($data['password_err']) || isset($data['email_err']) || isset($data['confirm_password_err']) || isset($data['phone_err'])) {


                $this->view('users/register', $data);
            } else {

                //Creación del usuario
                $insertado = $this->userModel->register($data);
            }

            //Checkeo de que se ha creado el usuario
            if ($insertado) {
                //Creación del mensaje flash y redirect
                $flash = new Flash();

                $flash->message('Ya estás registrado y puedes iniciar sesión', 'info');

                redirect("/users/login");
            }

            $this->view('users/register', $data);
        } else {

            //Creación del array $data e inicialización de las variables del array
            $data = [

                'name' => '',
                'password' => '',
                'confirm_password' => '',
                'email' => '',
                'email_err' => '',
                'phone' => '',

            ];

            $this->view('users/register', $data);
        }
    }

    /*---------------------------
    -     LOGIN USUARIO     -
    ----------------------------*/
    public function login()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $data['email'] = trim($_POST['email'], " ");
            $data['password'] = trim($_POST['password'], " ");


            if ($data['email'] == '') {

                $data['email_err'] = "Correo electrónico vacío. Introduzca uno";
            } else if ($this->userModel->findUserByEmail($data['email'])) {
            } else {

                $data['email_err'] = "El usuario no se ha encontrado";
            }

            if ($data['password'] == '') {

                $data['password_err'] = "Introduce una contraseña";
            } else if (strlen($data['password']) < 5) {

                $data['password_err'] = "La contraseña es menor a 6 caracteres. Escriba una con mayor longitud.";
            }



            $user = $this->userModel->login($data['email'], $data['password']);


            if ($user) {

                $this->createUserSession($user);
            } else {

                $this->view('/users/login', $data);
            }
        } else {

            $data = [

                'email' => '',
                'password' => '',



            ];

            $this->view('/users/login', $data);
        }
    }

    public function edit($id)
    {


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {


            $data['id'] = $id;
            $data['email_new'] = trim($_POST['email_new'], " ");
            $data['password_new'] = trim($_POST['password_new'], " ");
            $data['name_new'] = trim($_POST['name_new'], " ");
            $data['phone_new'] = trim($_POST['phone_new'], " ");

            //Sanitización del array $data
            filter_input_array(INPUT_POST, $data);

            if ($data['name_new'] == '') {

                $data['name_err'] = "Introduce un nombre";
            }

            if ($data['email_new'] == '') {

                $data['email_err'] = "Correo electrónico vacío. Introduzca uno";
            }

            if ($data['password_new'] == '') {

                $data['password_err'] = "Introduce una contraseña";
            } else if (strlen($data['password_new']) < 5) {

                $data['password_err'] = "La contraseña es menor a 6 caracteres. Escriba una con mayor longitud.";
            }
            if ($data['phone_new'] == '') {


                $data['phone_err'] = "Número de contacto no introducido, introduzca uno.";
            } else if (strlen($data['phone_new']) != 9) {

                $data['phone_err'] = "Número de contacto inválido";
            }

            if (isset($data['name_err']) || isset($data['password_err']) || isset($data['email_err']) || isset($data['phone_err'])) {

                $this->view('/users/edit/', $data);
                var_dump("hasta aqui llego");

            } else {

                $actualizado = $this->userModel->edit($data);
    
                var_dump($data);
                if ($actualizado) {

                    $flash = new Flash();
                    $flash->message('Usuario actualizado correctamente', 'info');
                    redirect('/routes/index');
                }else{

                    $this->view('users/edit',$data);
                }
            }
        } else {

            $data = [

                'name_new' => '',
                'email_new' => '',
                'password_new' => '',
                'phone_new' => '',
                'name_old' => '',
                'email_old' => '',
                'password_old' => '',
                'phone_old' => '',

            ];

            $user = $this->userModel->getUserById($id);

            $data['id'] = $id;
            $data['name_old'] = $user->name;
            $data['email_old'] = $user->email;
            $data['password_old'] = $user->password;
            $data['phone_old'] = $user->phone;

            $this->view('/users/edit', $data);
        }
    }


    /*---------------------------
    -     CREACIÓN SESIÓN      -
    ----------------------------*/
    public function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_name'] = $user->name;
        $_SESSION['user_email'] = $user->email;

        redirect('/routes/index');
    }


    /*---------------------------
    -     LOGOUT      -
    ----------------------------*/
    public function logout()
    {

        //Eliminar las variables de sesión del usuario logueado
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        //Destruir la sesión
        session_destroy();
        //Redirect al index
        redirect('/paginas/index');
    }

    public function deleteUser($id)
    {

        $this->userModel->deleteUser($id);
        //Eliminar las variables de sesión del usuario a eliminar
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);

        session_destroy();

        redirect('/paginas/index');
    }
}
